# sf_group_tableview

一个带有分区头和分区尾部、带有视图头部和视图尾部的listview，其中的写法类似于iOS中的tableview的写法，谁叫本人是一个iOS开发者呢！哈哈哈哈

## Getting Started

1.添加一个依赖到你的项目的pubspec.yaml文件
```
dependencies:
   sf_group_tableview: ^0.0.1
```
2.导入头文件
```
import 'package:sf_group_tableview/sf_group_tableview.dart';
```

3.使用SFGroupTableView
SFGroupTableView使用类似于iOS中的tableview，这对于有iOS开发经验的小伙伴很友好了，但是对于安卓同学就显示不是那么友好了，所以对于相关参数我需要解释解释的。
先说说此组件的简介：
是使用了iOS的tableview的设计思想，将listview分为了一个头部View和尾部View,这两者是不可以复用的，再将中间部分分成一个一个小的View，每个小的View成为一个分区，其中每个分区都有一个分区头部和分区尾部，中间是一个一个小的view，称为cell，这个分区头部和分区尾部还有中间的小cell都是可以复用的。
dataSource：是必须要实现的回调，他使用了TableViewDataSource这个类作为回调，类似于iOS中的代理方法，TableViewDataSource中有两必须要实现的参数：numberOfRowInSection和cellForRowAtIndexPath
      numberOfSections：可选，代表有几个分区，如果不实现默认有1个
      numberOfRowInSection：必选，代表对应的分区有多少个cell，
      cellForRowAtIndexPath：必选，代表每个分区对应位置要返回的cell
      
delegate：可选 ，使用TableViewDelegate这个类作为回调，
     headerForSection:可选， 返回的对应分区头部
     footerForSection：可选，返回对应分区尾部
     didSelectRowAtIndexPath：可选，点击对应位置cell的回调
     didSelectHeaderForSection：可选，点击对应分区头部的回调
     didSelectFooterForSection：可选，点击对应分区尾部的回调

header：可选，整个视图的头部
footer：可选，整个视图的尾部

itemHeight：可选，所有分区头部和尾部和cell的高度，不设置就是分区头部和分区尾部和cell的高度都是自适应的

其中方法中有一个参数IndexPath: 表示当前cell对应的位置，section代表对应的分区位置，row代表该分区下cell的位置，都是从0开始的
```
Scaffold(
      
      body: SFGroupTableView(
        header: () {
          return Container(
            // height: 100,
            color: Colors.blue,
            child: Text('我是头部视图'),
          );
        },
        footer: () {
          return Container(
            // height: 100,
            color: Colors.yellow,
            child: Text('我是尾部视图'),
          );
        },
        dataSource: TableViewDataSource(
          numberOfSections: 2,
          numberOfRowInSection: (section) {
            return 4;
          },
          cellForRowAtIndexPath: (indexPath) {
            return Container(
              // height: 40,
              color: Colors.pink,
              child: Text('哈哈'),
            );
          },
        ),
        delegate: TableViewDelegate(
          headerForSection: (section) {
            return Container(
              height: 100,
              color: Colors.purple,
              child: Text('我是分区头'),
            );
          },
          footerForSection: (section) {
            return Container(
              height: 80,
              color: Colors.green,
              child: Text('我是分区尾部'),
            );
          },
          didSelectRowAtIndexPath: (indexPath) {
            print('点击了cell${indexPath}');
          },
          didSelectHeaderForSection: (section) {
            print('点击了头=$section');
          },
        ),
        // itemHeight: 80,
      ),
      
    );

```
