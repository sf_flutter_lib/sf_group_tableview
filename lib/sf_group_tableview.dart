import 'package:flutter/material.dart';

/// 头部视图和尾部视图别名定义
typedef HeaderOrFooterCallBack = Widget Function();

/// 分区头部和分区尾部别名定义
typedef HeaderOrFooterForSectionCallBack = Widget Function(int section);

/// cell返回的别名定义
typedef CellForRowCallBack = Widget Function(IndexPath indexPath);

/// 每个分区有多少行的别名定义
typedef DataCountCallBack = int Function(int section);

/// cell固定高度的别名定义
typedef CellHeightCallBack = double Function(IndexPath indexPath);

/// 分区头或尾部固定高度的别名定义
typedef HeaderOrFooterForSectionHeightCallBack = double Function(int section);

/// cell的点击函数别名
typedef CellDidSelectCallBack = void Function(IndexPath indexPath);

/// 分区头或尾部点击函数的别名定义
typedef HeaderOrFooterForSectionDidSelectCallBack = void Function(int section);

enum IndexPathType {
  sectionHeader,
  row,
  sectionFooter,
}

/*************TableView***************/
///
// class a extends ListView {}

class SFGroupTableView extends ScrollView {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomScrollView(
        slivers: this._listSliver(),
      ),
    );
  }

  /***********实例变量**************/
  ///
  // Key key;
  TableViewDataSource dataSource;
  TableViewDelegate delegate;

  /// item的高度，包含了cell和分区的，如果不赋值，则高度自适应
  double itemHeight;
  HeaderOrFooterCallBack header;
  HeaderOrFooterCallBack footer;

  // 获取cell和分区总共的个数
  int get allItemCount => _allCount();

  /***********构造函数**************/
  ///
  /*
   * 构造函数
   * 
   * */
  SFGroupTableView({
    // this.key,
    @required this.dataSource,
    this.delegate,
    this.itemHeight,
    this.header,
    this.footer,
  });

  /***********初始化**************/

  /***********外部方法**************/

  /***********私有方法**************/

  //
  List<Widget> _listSliver() {
    return <Widget>[
      // 如果不是Slivers家族的Widget,需要使用SliverToBoxAdapter做层包裹
      (() {
        // 设置头部视图
        if (this.header != null) {
          Widget widget = SliverToBoxAdapter(child: this.header());

          return widget;
        }

        return SliverToBoxAdapter();
      })(),

      // 当列表项目高度固定时，使用SliverFiexdExtendList比SliverList具有更高的性能
      (() {
        SliverMultiBoxAdaptorWidget widget;
        if (this.itemHeight != null) {
          // 使用固定高度，且只要其中一个设置了固定高度，那么其他的都需要设置，
          widget = SliverFixedExtentList(
            delegate: this._sliverChildBuilderDelegate(),

            // 设置了cell的高度
            itemExtent: this.itemHeight,
          );
        } else {
          // 使用item自己的高度
          widget = SliverList(
            delegate: this._sliverChildBuilderDelegate(),
          );
        }

        return widget;
      })(),

      // 如果不是Slivers家族的Widget,需要使用SliverToBoxAdapter做层包裹
      (() {
        // 设置尾部视图
        if (this.footer != null) {
          Widget widget = SliverToBoxAdapter(child: this.footer());

          return widget;
        }

        return SliverToBoxAdapter();
      })(),
    ];
  }

  SliverChildBuilderDelegate _sliverChildBuilderDelegate() {
    return SliverChildBuilderDelegate(
      (context, index) {
        // index转成IndexPath
        IndexPath indexPath = this._indexPathFromIndex(index);
        if (indexPath.type == IndexPathType.sectionHeader) {
          // 分区头
          if (this.delegate?.headerForSection != null) {
            Widget widget;
            if (this.delegate?.didSelectHeaderForSection != null) {
              widget = GestureDetector(
                child: this.delegate.headerForSection(indexPath.section),
                onTap: () {
                  this.delegate.didSelectHeaderForSection(indexPath.section);
                },
              );
            } else {
              widget = this.delegate.headerForSection(indexPath.section);
            }

            return widget;
          }
        } else if (indexPath.type == IndexPathType.sectionFooter) {
          // 分区尾部
          if (this.delegate?.footerForSection != null) {
            Widget widget;
            if (this.delegate?.didSelectFooterForSection != null) {
              widget = GestureDetector(
                child: this.delegate.footerForSection(indexPath.section),
                onTap: () {
                  this.delegate.didSelectFooterForSection(indexPath.section);
                },
              );
            } else {
              widget = this.delegate.footerForSection(indexPath.section);
            }

            return widget;
          }
        } else {
          // 设置了datasource
          if (this.dataSource.cellForRowAtIndexPath != null) {
            Widget widget;
            if (this.delegate?.didSelectRowAtIndexPath != null) {
              widget = GestureDetector(
                child: this.dataSource.cellForRowAtIndexPath(indexPath),
                onTap: () {
                  this.delegate.didSelectRowAtIndexPath(indexPath);
                },
              );
            } else {
              widget = this.dataSource.cellForRowAtIndexPath(indexPath);
            }

            return widget;
          }
        }
        return SliverToBoxAdapter();
      },

      // 设置cell的个数和分区的总个数
      childCount: this.allItemCount,
    );
  }

  /// listView中的index   index代表listView中item的位置，从0开始
  IndexPath _indexPathFromIndex(int index) {
    IndexPath indexPath = IndexPath(0, 0, index: index);
    int sectionNum = 1; // 默认分区个数为1个
    // int rowsForSectionNum = 0; // 默认分区有0个cell

    int section = 0; // 分区位置
    int rowsForSection = 0; // 分区的row位置
    IndexPathType type = IndexPathType.row; // 默认为cell

    if (this.dataSource.numberOfSections != null) {
      // 证明有多个分区，至少一个，如果写小于等于0个小心报错
      sectionNum = this.dataSource.numberOfSections;
    }

    // 当前所有item的数量
    int currentAllItemNum = 0;
    // 上一个分区之前所有的item数量
    int lastAllItemNum = 0;
    for (var i = 0; i < sectionNum; i++) {
      // 当前section下的所有item的数量，包括分区头和尾部
      int currentSectionAllItemNum = countBySection(i);

      // 上一个section下的所有item的数量，包括分区头和尾部
      int lastSectionAllItemNum = 0;
      if (i > 0) {
        lastSectionAllItemNum = countBySection(i - 1);
        lastAllItemNum += lastSectionAllItemNum;
      }

      currentAllItemNum += currentSectionAllItemNum;
      if (index + 1 <= currentAllItemNum && index + 1 > lastAllItemNum) {
        // 如果此时的item位置在此分区内，则退出

        // 得出分区位置
        section = i;

        // 得出row的位置，和类型
        // 分区item的总数
        int sectionItemNum =
            currentSectionAllItemNum - (currentAllItemNum - (index + 1));
        // 分区头的数
        int tempSectionNum = 0;
        if (this.delegate?.headerForSection != null) {
          if (sectionItemNum == 1) {
            // rowsForSection = 0;
            type = IndexPathType.sectionHeader;
          }

          tempSectionNum = tempSectionNum + 1;
        }

        if (this.delegate?.footerForSection != null) {
          if (sectionItemNum == currentSectionAllItemNum) {
            // rowsForSection = 0;
            type = IndexPathType.sectionFooter;
          }
        }

        // 设置row
        rowsForSection = sectionItemNum - 1 - tempSectionNum;
        // print('个数===$tempSectionNum');
        // print(rowsForSection);
        break;
      }
    }

    // 设置最终IndexPath值
    indexPath = IndexPath(section, rowsForSection, type: type, index: index);
    return indexPath;
  }

  ///某一组的row数量,包括分区头和分区尾部
  int countBySection(int section) {
    // 获得某一分区的cell数量
    int amount = this.dataSource.numberOfRowInSection(section);

    if (this.delegate?.headerForSection != null) {
      amount += 1;
    }

    if (this.delegate?.footerForSection != null) {
      amount += 1;
    }
    return amount;
  }

  ///总item数量，包括header和footer
  int _allCount() {
    int count = 0;
    int sectionNum = 1; // 默认分区个数为1个
    int rowsForSectionNum = 0; // 默认分区有0个cell

    if (this.dataSource.numberOfSections != null) {
      // 证明有多个分区，至少一个，如果写小于等于0个小心报错
      sectionNum = this.dataSource.numberOfSections;
    }

    //
    int allSectionItemNum = 0;
    for (var i = 0; i < sectionNum; i++) {
      rowsForSectionNum = countBySection(i);
      allSectionItemNum += rowsForSectionNum;
    }

    count = allSectionItemNum;
    return count;
  }

  // @override
  // List<Widget> buildSlivers(BuildContext context) {
  //   // TODO: implement buildSlivers
  //   throw UnimplementedError();
  // }

  @override
  List<Widget> buildSlivers(BuildContext context) {
    return this._listSliver();
  }
}

/*************DataSource***************/
class TableViewDataSource {
  // Key key;

  /// 有多少个分区
  int numberOfSections;

  /// 每个分区有多少行
  DataCountCallBack numberOfRowInSection;

  /// cell
  CellForRowCallBack cellForRowAtIndexPath;

  /*
   * 构造函数
   * 
   * 
   * */
  TableViewDataSource({
    // this.key,
    this.numberOfSections,
    @required this.numberOfRowInSection,
    @required this.cellForRowAtIndexPath,
  });
}

/*************Delegate***************/
class TableViewDelegate {
  // Key key;

  // 设置分区头
  HeaderOrFooterForSectionCallBack headerForSection;

  // 设置分区尾部
  HeaderOrFooterForSectionCallBack footerForSection;

  // cell点击回调
  CellDidSelectCallBack didSelectRowAtIndexPath;

  /// 分区头部点击回调
  HeaderOrFooterForSectionDidSelectCallBack didSelectHeaderForSection;

  /// 分区尾部点击回调
  HeaderOrFooterForSectionDidSelectCallBack didSelectFooterForSection;

  TableViewDelegate({
    // this.key,
    this.headerForSection,
    this.footerForSection,
    this.didSelectRowAtIndexPath,
    this.didSelectHeaderForSection,
    this.didSelectFooterForSection,
  });
}

/**************IndexPath**************/
class IndexPath {
  IndexPath(this.section, this.row,
      {this.type = IndexPathType.row, this.index});

  int index; //原index
  int section;
  int row;
  IndexPathType type;

  @override
  String toString() {
    return "IndexPath(section:" +
        section.toString() +
        "," +
        "row:" +
        row.toString() +
        "," +
        "type:" +
        type.toString() +
        ",index:" +
        index.toString() +
        ")";
  }
}
